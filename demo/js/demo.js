$(function () {
	const storageString = "fittext-demo";
	const inputText = $(".js-text");
	const inputWidth = $(".js-width");
	const output = $(".js-output");

	var demoKeys = {"text" : inputText.val(), "width": inputWidth.val()};

	if (localStorage.getItem(storageString)) {
		demoKeys = JSON.parse(localStorage.getItem(storageString));
		inputText.val(demoKeys["text"]);
		inputWidth.val(demoKeys["width"]);
	}

	output.html(inputText.val());
	output.css("width", inputWidth.val() + "%");

	inputText.on("keyup", function(){
		output.html($(this).val());
		demoKeys["text"] = $(this).val();
		saveInSession();
	});

	inputWidth.on("input", function(){
		output.css("width", $(this).val() + "%");
		demoKeys["width"] = $(this).val();
		saveInSession();
	});

	var saveInSession = function (){
		localStorage.setItem(storageString, JSON.stringify(demoKeys));
	}
	
	$("#main").addClass("js-loaded");


});
