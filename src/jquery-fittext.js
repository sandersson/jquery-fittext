(function($){
	"use strict";

	$.fn.fitText = function(options) {

		var settings = $.extend({
            debug: false
        }, options);

		return this.each(function() {
			const output = $(this);

			//Save the initial font size = max font size
			const initialFontSize = output.css("font-size");
			var resizeId;

			var replica = $("<span />").html(output.html()).css({
				"white-space" : "nowrap",
				"font-family" : output.css("font-family"),
				"letter-spacing" : output.css("letter-spacing"),
				"font-size" : initialFontSize,
				"background" : settings.debug ? "green" : "transparent",
				"position" : settings.debug ? "static" : "absolute",
				"left" : settings.debug ? "inherit" : "-9999px"
			}).appendTo('body');

			var update = function(text) {
				if (settings.debug) {
					//Create a replica with same styling and text
					replica.html(text);
					replica.css("font-size", initialFontSize);
				} else {
					replica = $("<span />").html(text).css({
						"white-space" : "nowrap",
						"font-family" : output.css("font-family"),
						"letter-spacing" : output.css("letter-spacing"),
						"font-size" : initialFontSize,
						"background" : settings.debug ? "green" : "transparent"
					}).appendTo('body');
				}

				var min = 1,
    				max = 200,
    				fontSize;

    			do {
    				fontSize = (max + min) / 2;
    				replica.css('fontSize', fontSize);
    				var multiplier = output.height() / replica.height();
    				if (multiplier == 1) {
    					min = max = fontSize
    				}
    				if (multiplier > 1) {
    					min = fontSize
    				}
    				if (multiplier < 1) {
    					max = fontSize
    				}
    			} while ((max - min) > 1);
    			fontSize = min;

    			if (output.width() < replica.width()) {
    				min = 1;
    				do {
    					fontSize = (max + min) / 2;
    					replica.css('fontSize', fontSize);
    					var multiplier = output.width() / replica.width();
    					if (multiplier == 1) {
    						min = max = fontSize
    					}
    					if (multiplier > 1) {
    						min = fontSize
    					}
    					if (multiplier < 1) {
    						max = fontSize
    					}
    				} while ((max - min) > 1);
    				fontSize = min;
    			}

    			output.css({
    				"font-size": parseInt(fontSize),
    				"line-height": output.height() + "px"
    			});
    			if (!settings.debug)
					replica.remove();

			}

			var doneResizing = function(){
				update(output.html());
			}

			//Add MutationObserver to output to detect content and attribute changes
 			var observer = new MutationObserver(function(mutations) {
				mutations.forEach(function(mutation) {
					update(output.html());
				});    
			});
			 
			var config = { attributes: true, childList: true };
			output.each(function(){
				observer.observe(this, config);
			});

			//Detect window resize
			$(window).resize(function() {
				clearTimeout(resizeId);
				resizeId = setTimeout(doneResizing, 200);
			});
			
			update(output.html());

		});
	}

})(jQuery);
