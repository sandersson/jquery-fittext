# FitText.js

FitText.js is a jQuery plugin that resize font-sizes of selected elements to fit it's container.

## Simple implementation
~~~~
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="jquery-fittext.js"></script>
<script>
	$('#output').fitText();
</script>
~~~~

## Possible improvements
* Refactor MutationObserver due to unneccessary calls
* Add fallback to HTML5 range in demo to make it touch device friendly
* Add min and max font size to plugin options


